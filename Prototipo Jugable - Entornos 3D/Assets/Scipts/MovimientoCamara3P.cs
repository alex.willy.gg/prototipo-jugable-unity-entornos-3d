using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoCamara3P : MonoBehaviour
{
    public float rotationSpeed = 1f;
    public Transform Target, Player;
    float mouseX, mouseY;
    

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        cameraRotation();
        cameraColliderDetection();
    }

    void cameraRotation()
    {
        mouseX += Input.GetAxis("Mouse X") * rotationSpeed;
        mouseY -= Input.GetAxis("Mouse Y") * rotationSpeed;

        mouseY = Mathf.Clamp(mouseY, -20, 60);

        Target.rotation = Quaternion.Euler(mouseY, mouseX, 0f);
        Player.rotation = Quaternion.Euler(0, mouseX, 0f);
    }

    void cameraColliderDetection()
    {

    }
}
