using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPersonaje3P : MonoBehaviour
{
    public float speed = 6f; 

    // Update is called once per frame
    void Update()
    {
        playerMovement();
    }

    void playerMovement()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 playerMovement = new Vector3(horizontal, 0f, vertical) * speed * Time.deltaTime;

        transform.Translate(playerMovement, Space.Self);
    }
}
